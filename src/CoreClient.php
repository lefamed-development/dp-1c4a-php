<?php

namespace LEFAMED\OneClickForApplication;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class CoreClient
{
    const CONFIG_PARTNERID          = 'partnerID';
    const CONFIG_KEYPHASE           = 'keyPhase';
    const CONFIG_PRIVATEKEY         = 'privateKey';

    const SOAP_HEADER_NAMESPACE     = 'http://oneclickforpartner.dpag.de';

    /**
     * @var null|CoreClient
     */
    private static $instance = null;

    private $config;
    private $soap;
    private $token = null;
    private $walletBalance = null;
    private $showTermsAndConditions = null;

    /**
     * @return CoreClient
     */
    public static function getInstance()
    {
        if(static::$instance === null)
            static::$instance = new static();

        return static::$instance;
    }

    /**
     * Deny object cloning
     */
    private function __clone() {}

    /**
     * CoreClient constructor.
     */
    private function __construct()
    {
        //read configuration from app level configuration
        $this->config = config('services.1c4a');

        //create a new soap client instance
        $this->soap = new \SoapClient($this->config['wsdl'], [
            'trace' => 1
        ]);

        if(!$this->token)
            $this->authenticate();

    }

    /**
     * Returns pretty formatted date string to use with the api.
     *
     * @param Carbon $dateTime
     * @return string
     */
    private function formatDate(Carbon $dateTime)
    {
        return $dateTime->format('dmY-His');
    }

    /**
     * Starts authentication and retrieves an user token.
     */
    public function authenticate()
    {
        //run authentication
        $res = $this->authenticateUser([
            'username' => $this->config['username'],
            'password' => $this->config['password']
        ]);

        if($res) {
            //save the result vars
            $this->token = $res->userToken;
            $this->walletBalance = $res->walletBalance;
            $this->showTermsAndConditions = $res->showTermsAndConditions;
        }
    }

    private function buildSoapHeaders()
    {
        //generate a timestamp for the request
        $requestTimestamp = $this->formatDate(Carbon::now());

        //get the hash base (-> unhashed value to get hashed)
        $hashBase = implode(
            '::',
            array(
                trim($this->config[static::CONFIG_PARTNERID]),
                trim($requestTimestamp),
                trim($this->config[static::CONFIG_KEYPHASE]),
                trim($this->config[static::CONFIG_PRIVATEKEY])
            )
        );

        //generate the signature
        $partnerRequestSignature = substr(md5($hashBase), 0, 8);
        
        return array(
            new \SoapHeader(static::SOAP_HEADER_NAMESPACE, 'PARTNER_ID', $this->config[static::CONFIG_PARTNERID]),
            new \SoapHeader(static::SOAP_HEADER_NAMESPACE, 'REQUEST_TIMESTAMP', $requestTimestamp),
            new \SoapHeader(static::SOAP_HEADER_NAMESPACE, 'KEY_PHASE', $this->config[static::CONFIG_KEYPHASE]),
            new \SoapHeader(static::SOAP_HEADER_NAMESPACE, 'PARTNER_SIGNATURE', $partnerRequestSignature)
        );
    }

    /**
     * @return bool
     */
    public function getPreviewVoucher($productCode)
    {
        $res = $this->retrievePreviewVoucherPNG([
            'productCode' => $productCode,
            'voucherLayout' => 'FrankingZone'
        ]);

        if($res)
            return $res->link;
        else
            return false;
    }

    /**
     * Generates an order id
     */
    public function prepareOrder()
    {
        $res = $this->createShopOrderId([
            'userToken' => $this->token
        ]);

        if($res)
            return $res->shopOrderId;
        else
            return false;
    }


    /**
     * Returns a voucher
     *
     * @param $productId
     * @return mixed
     */
    public function getVoucher($productId)
    {
        //starting creating a order id
        $orderId = $this->prepareOrder();

        //checkout vouchers
        $res = $this->checkoutShoppingCartPNG([
            'userToken' => $this->token,
            'shopOrderId' => $orderId,
            'positions' => [
                [ 'productCode' => $productId, 'voucherLayout' => 'FrankingZone' ]
            ],
            'total' => 45
        ]);

        return $res;
    }

    /**
     * @param $name
     * @param $arguments
     * @return bool|mixed
     */
    function __call($name, $arguments)
    {
        //calculate the soap headers with the hash
        $this->soap->__setSoapHeaders($this->buildSoapHeaders());

        try {
            //call function
            return $this->soap->__call($name, $arguments);
        } catch(\Exception $e) {
            dump($this->soap->__getLastRequestHeaders());
            dump($this->soap->__getLastRequest());
            dump($this->soap->__getLastResponse());

            dump($e->getMessage());

            return false;
        }

    }


}
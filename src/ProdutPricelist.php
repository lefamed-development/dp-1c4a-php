<?php

namespace LEFAMED\OneClickForApplication;

use LEFAMED\OneClickForApplication\Enums\ProductCodeEnum;

/**
 * Class ProductPricelist
 *
 * Defines product pricing
 *
 * @package LEFAMED\OneClickForApplication
 */
class ProductPricelist
{
    /**
     * Defines the prices
     *
     * @var array
     */
    public static $pricelist = [
        ProductCodeEnum::POSTCARD_DE => 0.45
    ];

    /**
     * Returns the price for selected product
     *
     * @param $productId
     * @return mixed
     * @throws \Exception
     */
    public static function getPrice($productId)
    {
        if(!isset(static::$pricelist[$productId]))
            throw new \Exception('Price for product ' . $productId . ' not defined!');

        return static::$pricelist[$productId];
    }
}
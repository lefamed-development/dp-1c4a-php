<?php

namespace LEFAMED\OneClickForApplication;

use Illuminate\Support\ServiceProvider;

/**
 * Class OneClickForApplicationProvider
 * @package LEFAMED\OneClickForApplication
 */
class OneClickForApplicationProvider extends ServiceProvider
{
    public function boot()
    {}
}